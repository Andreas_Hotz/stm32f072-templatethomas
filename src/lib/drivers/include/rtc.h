/*
 *****************************************************************************
 * FILE:			rtc.h
 *
 *
 * VERSION: 		1.0
 * AUTHOR: 			Christian Angst
 *
 * DESCRIPTION: 	RTC Stuff for STM32F0 discovery board
 * MODIFICATION:
 *****************************************************************************
*/

#ifndef __RTC_H__
#define __RTC_H__

#include <stdbool.h>


bool rtc_init();
void rtc_date_time_show(void);

void rtc_time_get(uint8_t* hours, uint8_t* minutes, uint8_t* seconds);
void rtc_time_set(uint8_t hours, uint8_t minutes, uint8_t seconds);
void rtc_date_set(uint8_t day, uint8_t month, uint8_t year);
void rtc_date_get(uint8_t* day, uint8_t* month, uint8_t* year);

#endif
